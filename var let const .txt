let и const дают блочную область видимости
Это означает, что область видимости при их использовании может быть ограничена блоком кода, 
таким, как цикл for или выражение if.
Это даёт разработчику больше гибкости в выборе областей видимости переменных.
Ключевое слово const очень похоже на let, но у них есть одно важное различие. 
Это ключевое слово используется для объявления констант. 
Значения констант после их инициализации менять нельзя. 
Нужно отметить, что это относится лишь к значениям примитивных типов, воде строки или числа. 
Если константа является чем-то более сложным, например — объектом или массивом,
внутреннюю структуру подобной сущности модифицировать можно, нельзя лишь заменить её саму на другую.
let и const, дают больше возможностей по контролю за областью видимости переменных (и констант) 
в коде веб-сайтов и веб-приложений. Они заставляют больше думать о том, как именно работает код,
а такие размышления хорошо влияют на то, что получается.